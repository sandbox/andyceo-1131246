<?php
// $Id$

/**
 * @file
 * Hooks provided by the private_room module.
 */

/**
 * Add additional functions to generate view.
 *
 * @return
 *   An array of arrays of functions with params and names.
 */
function hook_widgetlist() {
  return array(
    array('name' => '_private_room_default_widget', 'desc' => 'Default', 'params' = array()),
  );
}