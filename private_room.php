<?php

/******FUNCTION DEFINITION*******/

//copypasted from Drupal bootstrap.inc
function drupal_validate_utf8($text) {
  if (strlen($text) == 0) {
    return TRUE;
  }
  return (preg_match('/^./us', $text) == 1);
}

function check_plain($text) {
  return drupal_validate_utf8($text) ? htmlspecialchars($text, ENT_QUOTES) : '';
}
//end copypasting from Drupal bootstrap.inc

/******CUSTOM FUNCTIONS STARTED*****/

function throw_error($msg) {
    print $msg;
    exit;
}

function db_connect() {
global $db_url;
    $db = parse_url ($db_url);
    // ����������� � ����
    if (@!mysql_connect($db['host'], $db['user'], $db['pass']) ){
        throw_error('Cant connect to db server: '.mysql_error());
    }
    if (@!mysql_select_db (substr ($db['path'], 1))){
        throw_error('Cant select db: '.mysql_error());
    }
    return TRUE;
}

function db_query($sql){
static $is_connect = FALSE;
    if(!$is_connect) db_connect();

    $result = @mysql_query($sql);
    if(!$result) return FALSE;
    return $result;
}

function db_result_array(&$result){
    return mysql_fetch_assoc($result);
}



/*********************************************************/



if( preg_match('/^(.+\/)sites\//',$_SERVER['SCRIPT_FILENAME'], $tmp)){
    $root = $tmp[1];
    $path = $root.'sites/'.$_SERVER['SERVER_NAME'].'/';
    unset($tmp);
}else  throw_error('Cant fide drupal root path');

// ���� �����������
include_once($path.'settings.php');



/*********************************************************************************/
// �������� ID � ������ �������� ������������
/*
    ������������� ������ �������� � ����
    ��� ���� ��������� �� ������ �������� � ����� ��� �� ��������: http://api.drupal.org/api/function/conf_init/5
    �������� ����������:
*/
/*$sid = 0;
$prefix_cookie = 'SESS';
$pattern = '/^'.$prefix_cookie.'[0-9a-zA-Z]{32}$/';
foreach ($_COOKIE as $name => $value){
    if (substr($name, 0, strlen($prefix_cookie)) == $prefix_cookie && preg_match ($pattern, $name)){
        if( preg_match( '/^[0-9a-z]{32}$/i', $value)){
            $sid = $value; break;
        }
    }
}*/

/*
  ���. �� ����� �������� ��� ������ ���, ��� ��� ������ ������.
  ���� �������� ���� �� conf_init() � ����� includes/bootstrap.inc, line 249 
*/
  if (!$cookie_domain) {
    // If the $cookie_domain is empty, try to use the session.cookie_domain.
    $cookie_domain = ini_get('session.cookie_domain');
  }
  if ($cookie_domain) {
    // If the user specifies the cookie domain, also use it for session name.
    $session_name = $cookie_domain;
  }
  else {
    // Otherwise use $base_url as session name, without the protocol
    // to use the same session identifiers across http and https.
    list( , $session_name) = explode('://', $base_url, 2);
    // We try to set the cookie domain to the hostname.
    // We escape the hostname because it can be modified by a visitor.
    if (!empty($_SERVER['HTTP_HOST'])) {
      $cookie_domain = check_plain($_SERVER['HTTP_HOST']);
    }
  }
  // To prevent session cookies from being hijacked, a user can configure the
  // SSL version of their website to only transfer session cookies via SSL by
  // using PHP's session.cookie_secure setting. The browser will then use two
  // separate session cookies for the HTTPS and HTTP versions of the site. So we
  // must use different session identifiers for HTTPS and HTTP to prevent a
  // cookie collision.
  if (ini_get('session.cookie_secure')) {
    $session_name .= 'SSL';
  }
/*  ����� ���������.  */

$session_name = 'SESS'. md5($session_name);
$sid = $_COOKIE[$session_name];


$uid = 0; 
if ($sid){
    $tb_name = $db_prefix.'sessions';
    $sql = "SELECT uid, session FROM {$tb_name} WHERE sid = '{$sid}' LIMIT 1";
    $row = db_result_array(db_query($sql));

    $uid = $row['uid']; //$SESSION = unserialize($arr['session']);
    // � ���� �� ������ ���� � ���������� ���������������
    if (is_null($uid)) $uid = 0; 
}
/*********************************************************************************/


// id �������������� �����
if (!isset($_GET['id']) || (int)$_GET['id'] == 0) throw_error ('wrong file id');
$fid = (int)$_GET['id'];



/* ��������� ����� */
$users_roles = $db_prefix.'users_roles';
$frp = $db_prefix.'private_room_frp';
$fup = $db_prefix.'private_room_fup';

if($uid == 0) {
  $sql = "SELECT SUM(perm) AS flag FROM {$frp} WHERE fid = '{$fid}' AND rid = '1'";
} else {
  $rids = array(1, 2);
  
  $sql = "SELECT rid FROM {$users_roles} WHERE uid='{$uid}'";
  $result = db_query($sql);
  while($row = db_result_array($result))
    $rids[] = $row['rid'];
  
  $sql = "SELECT SUM(frp.perm) AS flag FROM {$frp} AS frp WHERE frp.fid = '{$fid}' AND frp.rid IN (". join($rids, ',').")
          UNION SELECT SUM(fup.uperm) AS flag FROM {$fup} AS fup WHERE fup.fid = '{$fid}' AND fup.uid = '{$uid}'
          ";
}

$result = db_query($sql);
$success = 0;
while ($row = db_result_array($result)) {
  $success += (int)$row['flag'];
}

/* ��� ���� �� ������ ����� */
if(!$success) {
    header('HTTP/1.0 403 Forbidden');
    throw_error('<h1>403 Forbidden</h1>');
}


/* ��������� ���� �� ���� */
$tb_files = $db_prefix.'files';
$sql = "SELECT * FROM {$tb_files} WHERE fid = '{$fid}'";
$file = db_result_array(db_query($sql));

$file_here = $root.$file['filepath'];
//dump::trace($file);

// ����� ��� � �������� �������
if( !file_exists($file_here) ){
    header('HTTP/1.0 404 Not Found');
    throw_error('<h1>404 Not Found</h1>');
}

// ���-�� ���� � ������
$tb_vars = $db_prefix.'variable';
$sql = "SELECT value FROM {$tb_vars} WHERE name = 'private_room_speed'";
$speed = db_result_array(db_query($sql));
$speed = @unserialize($speed['value']);
if(!$speed) $speed = 50; // must be PRIVATE_ROOM_SPEED_DEFAULT !!!
$chop = $speed * 1024;


$fd = @fopen($file_here, 'rb');
if(!$fd){
    header('HTTP/1.0 503 Service Unavailable');
    throw_error('<h1>503 Service Unavailable</h1>');
}

$total_size = filesize($file_here);

set_time_limit(0);
header('HTTP/1.0 200 OK');
header('Accept-Ranges: bytes');
header('Content-length: '. $total_size);
header('Content-type: '. $file['filemime']);
//header('Content-type: application/octet-stream');
header('Content-disposition: attachment; filename="'. $file['filename'] .'"'); 
ob_start();


$readed = 0;
while($readed < $total_size){
    $content = fread($fd, $chop);
    $readed+= $chop;
    print $content;
    flush(); ob_flush();
    sleep(1);
}
fclose ($fd);
exit;