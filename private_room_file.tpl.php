<?php
global $base_url;
$type = strrchr($file->filename, '.');
$url = $base_url . '/' . drupal_get_path('module', 'private_room') . '/private_room.php' . '?id=' . $file->fid;
$str = l($file->title, $url)." <span>({$type}, {$file->filesize} bytes)</span>";
echo $str;