<?php

/***************HELPER FUNCTIONS*********************/

function _private_room_get_roles() {
  //����� ������. �� ����� ������ �����
  $sql = "SELECT * FROM {role} ORDER BY name ASC";
  $res = db_query($sql);
  $roles = array();
  while($row = db_fetch_object($res))
    $roles[$row->rid] = $row->name;
  return $roles;
}

function _private_room_get_groups_array() {
  //����� ������������� ������ ���� �����, � ������� ���� �����
  $sql = "SELECT fg.gid, g.name FROM {private_room_fg} AS fg, {private_room_groups} AS g WHERE fg.gid = g.id ORDER BY g.name";
  $res = db_query($sql);
  $groupes = array();
  while($row = db_fetch_object($res))
    $groupes[$row->gid] = $row->name;
  return $groupes;
}

function _private_room_get_groups($pid = 0, $level = 0) {
  $sql = "SELECT * FROM {private_room_groups} WHERE pid = %d ORDER BY weight ASC";
  $res = db_query($sql, $pid);
  $groups = array();
  while($row = db_fetch_object($res)) {
    $hyphens = '';
    for($i=0;$i<$level;$i++) $hyphens .= '--';
    $row->name = $hyphens . $row->name;
    
    $groups_recurse = _private_room_get_groups($row->id, $level+1);
    
    $groups[$row->id.'_'] = $row->name;
    
    if(count($groups_recurse)) $groups = array_merge($groups, $groups_recurse);
  }
  return $groups;
}

function _private_room_get_groups_select($noelement = array()) {
  if(is_array($noelement) && !count($noelement)) $noelement = array('0_' => t('No'));
  
  //is noelement exists?
  if($noelement === NULL || $noelement === FALSE) $temp = _private_room_get_groups();
  else $temp = array_merge($noelement, _private_room_get_groups());
  
  foreach($temp as $k_=>$v) {
    $k = str_replace('_', '', $k_);
    unset($temp[$k_]);
    $temp[$k] = $v;
  }
  return $temp;
}

function _private_room_widgetlist($op = 'get_func_full_list', $num = NULL) {
  static $functions = NULL;
  
  if(is_null($functions)) {
    $functions = array(array('name' => '_private_room_default_widget', 'desc' => 'Default', 'params' => array()));

    // Hook other modules for adding additional lines.
    if ($additions = module_invoke_all('widgetlist'))
      $functions = array_merge($functions, $additions);
    unset($additions);
    foreach($functions as $k=>$f)
      $functions[$k]['params'] = serialize($f['params']);
  }
  
  switch($op){
    case 'get_func_select_list':
      $funcs = array();
      foreach ($functions as $n => $f) {
        if(empty($f['name'])) {
          drupal_set_message(t('Function is empty: %f', array('%f'=>$f['name'].'()')), 'warning', FALSE);
          continue;
        }
        elseif(empty($f['desc'])) {
          drupal_set_message(t('Function description is empty: %f', array('%f'=>$f['desc'].'()')), 'warning', FALSE);
          continue;
        }
        elseif(!function_exists($f['name'])) {
          drupal_set_message(t('Using undefined function detected: %f', array('%f'=>$f['name'].'()')), 'warning', FALSE);
          continue;
        }
        $funcs[$n] = $f['desc'];
      }
      return $funcs;
      break;
    case 'get_func_name_by_num':
      if(is_numeric($num)) return $functions[$num];
      else return FALSE;
      break;
    case 'get_func_full_list':
    default:
      return $functions;
  }
}

function _private_room_fg_link($file, $gid, $title) {
  $fid = $file->fid;
  $sql = "INSERT INTO {private_room_fg} (fid, title, gid) VALUES (%d, '%s', %d)";
  db_query($sql, $fid, $title, $gid);
}


/**
 * Callback to inject an .htaccess file into the private_upload_path folder
 */
function _private_room_add_htaccess() {
  $htaccess = "SetHandler Drupal_Security_Do_Not_Remove_See_SA_2006_006
Deny from all";
  $hta_path = _private_room_get_path() .'/.htaccess';
  $path = file_create_path($hta_path);
  if(file_save_data($htaccess, $path, FILE_EXISTS_REPLACE)) chmod($path, 0664);
  drupal_set_message("Added .htaccess file at $path");
}

/**
 * Generate path to the private folder
 */
function _private_room_get_path() {
  return file_directory_path() .'/'. PRIVATE_ROOM_FOLDER;
}

/**
 *  �������� ����� � �������� ������
 */
function _cut_str($str, $len, $suffix = '�') {
    $str = trim($str);
    $l = mb_strlen($str);
    if ($l <= $len) return $str;
    
    if ( $suffix == '�') mb_convert_variables ('UTF-8', 'ASCII, WINDOWS-1251', $suffix);

    $lp = mb_strlen($suffix);
    $short_str = trim (mb_substr ($str, 0, $len - $lp)) . $suffix;
    return $short_str;
}

/**
 *  ������ ������ �����
 */
function _private_room_group_tree() {
    // ������ ������ �����
    $tree = array();
    $tree['_index'] = array(); // ������ �����
    $sql = "SELECT id, pid, name FROM {private_room_groups} WHERE 1 ORDER BY weight, pid, name ASC";
    $result = db_query($sql);
    while ($row = db_fetch_object($result)){
        $tree['_index'][$row->id] = $row;
        $tree[$row->pid][$row->id] =& $tree['_index'][$row->id];
        $tree[$row->pid][$row->id]->_childs =& $tree[$row->id];
        $tree[$row->pid][$row->id]->_files = array();
    }
    return $tree;
}

/**
 * ������� ������ ����� ������� ���������.
 */
function _private_room_edit_table_recurs($tree, $level = 0) {
  $tb_rows = array();
  if (is_array($tree)) {
    foreach ($tree as $group) {
      // ������� ������ � �������
      // ���� ���� ����� � ������ ��� ��������� - ������ ������ Delete
      $actions_group = l(t('Edit'), 'private_room/edit/group/'.$group->id.'/edit');
      $class_group = 'group-name';
      if( !$group->_childs && count($group->_files) == 0){
        $actions_group .= ' | '. l(t('Delete'), 'private_room/edit/group/'.$group->id.'/delete');
        $class_group .= ' group-empty';
      }
      $tb_rows[] = array(
        array('data' => str_repeat('<span class="extend">&nbsp;', $level).'<strong>'. $group->name .'</strong>'.str_repeat('</span>', $level), 'class' => $class_group),
        array('data' => $actions_group),
      );
      // ������� ����� ������
      
      foreach($group->_files as $file) {
        $tb_rows[] = array(
          array('data' => str_repeat('<span class="extend">&nbsp;', $level). $file->title .str_repeat('</span>', $level)),
          array('data' => l(t('Edit'), 'private_room/edit/file/'.$file->fid.'/edit') .' | '. l(t('Delete'), 'private_room/edit/file/'.$file->fid.'/delete')),
        );
      }
      // ���� ���� ��������� ������������ �� � ��������
      if ($group->_childs) {
        $childs = _private_room_edit_table_recurs($group->_childs, $level + 1);
        $tb_rows = array_merge($tb_rows, $childs);
      }
    }
  }
  
  return $tb_rows;
}



/******************FORMS****************************/
function _private_room_admin_form() {
  $form = array();
  
  $form['page_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page settings'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['page_settings']['private_room_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title of the download page.'),
    '#default_value' => variable_get('private_room_title', PRIVATE_ROOM_TITLE),
    '#required' => TRUE,
    '#weight' => -10,
  );
  
  $form['download_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Download settings'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  
  $form['download_settings']['private_room_speed'] = array(
    '#type' => 'textfield',
    '#title' => t('Download speed'),
    '#description' => t('Download speed for the user, Kb/sec. From !from Kb/sec to !to Kb/sec.',array('!from'=>PRIVATE_ROOM_SPEED_FROM, '!to' => PRIVATE_ROOM_SPEED_TO)),
    '#default_value' => variable_get('private_room_speed', PRIVATE_ROOM_SPEED_DEFAULT),
    '#required' => TRUE,
    '#weight' => -10,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 7,
  );
  
  return $form;
}

function _private_room_add_file_form() {
  $form = array();
  
  $form['#attributes']['enctype'] = 'multipart/form-data';
  $form['#redirect'] = 'private_room/permissions';
  
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    //'#description' => t('Upload a file to your private room.'),
    '#required' => TRUE,
    '#weight' => -15,
  );
  
  $form['new_file'] = array(
    '#type' => 'file',
    '#title' => t('New file'),
    '#description' => t('Upload a file to your private room.'),
    '#weight' => -10,
  );
  
  $form['group'] = array(
    '#type' => 'select',
    '#title' => t('Group'),
    '#description' => t('Choose the group to download the file.'),
    '#weight' => -9,
    '#default_value' => 0,
    '#options' => _private_room_get_groups_select(FALSE),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => -6,
  );
  
  return $form;
}

function _private_room_edit_file_form($form_state, $fid) {

    $sql = "SELECT * FROM {private_room_fg} WHERE fid = %d";
    $result = db_query($sql, $fid);
    $file = db_fetch_object($result);
    
    $form = array();
    $form['fid'] = array(
        '#type' => 'hidden',
        '#value' => $file->fid,
    );
    $form['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Title'),
        '#required' => TRUE,
        '#default_value' => $file->title,
        '#weight' => -15,
    );
    $form['gid'] = array(
        '#type' => 'select',
        '#title' => t('Group'),
        '#description' => t('Choose the group for the file.'),
        '#weight' => -9,
        '#default_value' => $file->gid,
        '#options' => _private_room_get_groups_select(FALSE),
    );
    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#weight' => -6,
    );
    
    $form['#redirect'] = 'private_room/edit';
    
    return  $form;
}

function _private_room_add_group_form() {
  $form = array();
  
  $form['group_name'] = array(
    '#description' => t('Enter the group name.'),
    '#required' => TRUE,
    '#weight' => -11,
    '#type' => 'textfield',
    '#title' => t('Group name'),
    '#maxlength' => 255,
  );
  
  $form['group_parent'] = array(
    '#type' => 'select',
    '#title' => t('Parent group'),
    '#description' => t('Choose the parent group.'),
    '#weight' => -10,
    '#default_value' => 0,
    '#options' => _private_room_get_groups_select(),
  );
  
  $form['group_weight'] = array(
    '#type' => 'weight',
    '#title' => t('Group weight'),
    '#default_value' => 0,
    '#delta' => 10,
    '#weight' => -9,
    '#description' => t('Choose the group weight. Used for sorting.'),
  );
  
  $form['group_desc'] = array(
    '#weight' => -8,
    '#description' => t('Enter the short description for that group.'),
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#maxlength' => 255,
  );
  
  $form['group_widget'] = array(
    '#required' => TRUE,
    '#multiple_toggle' => TRUE,
    '#weight' => -7,
    '#type' => 'select',
    '#title' => t('Widget type'),
    '#options' => _private_room_widgetlist('get_func_select_list'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => -6,
  );
  
  return $form;
}

function _private_room_edit_group_form($form_state, $id) {

    $sql = "SELECT * FROM {private_room_groups} WHERE id = %d";
    $result = db_query($sql, $id);
    $group = db_fetch_object($result);
    
    $form = _private_room_add_group_form();
    $form['id'] = array(
        '#type' => 'hidden',
        '#value' => $group->id,
    );
    $form['group_name']['#default_value'] = $group->name;
    $form['group_parent']['#default_value'] = $group->pid;
    $form['group_weight']['#default_value'] = $group->weight;
    $form['group_desc']['#default_value'] = $group->description;
    foreach(_private_room_widgetlist() as $key => $widget);
        if ($widget->name == $group->function){
            $form['group_widget']['#value'] = $key;
            break;
        }
    
    $form['#redirect'] = 'private_room/edit';
    
    return $form;
}

function private_room_group_permissions_form() {
    // ����
    $rols = array();
    foreach(_private_room_get_roles() as $rid => $name)
        $rols[$rid] = '';

    
    // ������ ���� ��������� ������ �� ���������� �� �������
    // ������ - ������ ������ ��� �����
    $files = array('#index' => array());  
    // ������ ������   
    $sql = "SELECT fg.fid, fg.title, g.name AS `group`";
    $sql.= " FROM {private_room_fg} AS fg, {private_room_groups} as g ";
    $sql.= " WHERE fg.gid = g.id ORDER BY g.weight, g.name, fg.title ASC";
    $result = db_query($sql);
     while($row = db_fetch_object($result)) {
        $files[$row->group][$row->fid] = array(
            'title' => $row->title,
            'rids' => array(),
        );
        $files['#index'][$row->fid] =& $files[$row->group][$row->fid];
    }

    // ���� ��� ������� �����
    $sql = "SELECT * FROM {private_room_frp} WHERE 1";
    $result = db_query($sql);
    while( $row = db_fetch_object($result)){
        $files['#index'][$row->fid]['rids'][] = $row->rid; 
    }
    
    
    /****************************************************************/
    // ��������� �����
    $form = array();
    $form['titles'] = array();
    $form['rids'] = array();
    $form['rids']['#tree'] = TRUE;
 
    $group_i = 0;
    foreach($files as $name => $group){
        if($name == '#index') continue;
        $form['titles']['$'.$group_i] = array('#value' => $name);
        $group_i++;
        foreach($group as $fid => $file){
            $form['titles'][$fid] = array('#value' => $file['title']);
            $form['rids'][$fid] = array(
                '#type' => 'checkboxes',
                '#options' => $rols,
                '#value' => array(),
            );
             // ���������� ���������� ����
            foreach($file['rids'] as $rid)
                $form['rids'][$fid]['#value'][$rid] = TRUE;
        }
    }
    
    $form['submit'] = array(
        '#type' => 'submit', '#value' => t('Save'),
    );
    $form['#theme'] = 'private_room_group_permissions_form_render';
    return $form;    
}

function private_room_user_permissions_form($form_state, $uid) {
  if(!is_numeric($uid)) return array();
  
  $sql = "SELECT fg.fid, fg.title, fup.uperm
          FROM {private_room_fg} AS fg
          LEFT JOIN {private_room_fup} AS fup ON fg.fid = fup.fid AND fup.uid = %d
          WHERE 1";
  $res = db_query($sql, $uid);
  
  $files = array();
  $files_en = array();
  while($row = db_fetch_object($res)) {
    $files[$row->fid] = $row->title;
    if($row->uperm) $files_en[] = $row->fid;
  }
  
  $account = user_load(array('uid'=>$uid));
  
  $form = array(
    'uperms' => array(
      '#type' => 'checkboxes',
      '#title' => t('Files for the user @username', array('@username'=>$account->name)),
      '#default_value' => $files_en,
      '#options' => $files,
      '#description' => t('Select files, accessible for the user <a href="/user/@uid">%username</a>', array('%username'=>$account->name,'@uid' => $uid)),
    ),
    
    'account' => array(
      '#type' => 'value',
      '#value' => $account,
    ),
    
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save'),
    ),
    
    //form settings
    '#redirect' => 'private_room/edit/uperm',
    
  );
  return $form;
}



/******************THEMING FORMS***********************/
function theme_private_room_group_permissions_form_render($form) {
     // ��������� �������
    $tb_head = array ();
    $tb_head[] = array ('data' => t('Title'), 'title' =>  t('Title'), 'width' => '30%');
    foreach(_private_room_get_roles() as $rid => $name)
        $tb_head[] = array ('data' => _cut_str(t($name), 10), 'title' => t($name));

        
    //  �������� � ��������
    $tb_body = array();
    foreach ($form['titles'] as $fid => $title){
        // ���������� �������� ��������
        if ( substr($fid, 0, 1) == '#') continue;
        // ������ ��� ����������
        $item =& $form['rids'][$fid];

        $tb_row = array(); // ������ �������
        
        if( substr($fid, 0, 1) == '$'){
            $tb_row[] = array(
                'data' => '<strong>'.drupal_render( $form['titles'][$fid]).'</strong>', 
                'colspan' => '50', 'class' => 'files-group');
            $tb_body[] = $tb_row;
            continue;
        }
        
        $tb_row[] = array('data' => drupal_render( $form['titles'][$fid] ) );
        foreach (element_children($item) as $key )
           $tb_row[] = array( 'data' => drupal_render($item[$key]));

        $tb_body[] = $tb_row;
    }

    $str = theme_table($tb_head, $tb_body, array(), t('Files permissions for the users groups'));
    $str .= drupal_render($form);

    return '<div class="private-room private-room-permissions">'. $str .'</div>';
}

/*function theme_private_room_user_permissions_form_render($form) {
}*/



/******************FORMS VALIDATE***********************/
function _private_room_admin_form_validate($form, &$form_state) {
  $speed = (int)$form_state['values']['private_room_speed'];
  if(!$speed) form_set_error('private_room_speed', t('Please, enter the integer!'));
  elseif(PRIVATE_ROOM_SPEED_FROM > $speed || $speed > PRIVATE_ROOM_SPEED_TO)
    form_set_error('private_room_speed', t('The integer must be from !from to !to!', array('!from' => PRIVATE_ROOM_SPEED_FROM, '!to' => PRIVATE_ROOM_SPEED_TO)));
  $form_state['storage']['private_room']['speed'] = $speed;
}

function _private_room_add_group_form_validate($form, &$form_state) {
  //check the 'group_widget' field
  if(is_numeric($form_state['values']['group_widget']))
    $form_state['storage']['function'] = _private_room_widgetlist('get_func_name_by_num',$form_state['values']['group_widget']);
  else form_set_error('group_widget', t('Wrong widget!'));
}

function _private_room_edit_group_form_validate($form, &$form_state) {
    _private_room_add_group_form_validate($form, $form_state);
    // ������ ���������� ������ � ���� �������� ������
    $tree = _private_room_group_tree();
    $parent = $tree['_index'][$form_state['values']['group_parent']];
    // ����������� ����� �� ������ ��������� �� �����
    while ($parent->id != 0){
        // ���� ����� �� ��������� �������� �� ���� - ���������� ������
        if ($parent->id == $form_state['values']['id']){
            form_set_error('group_parent', t('Can\'t move the group into their childs'));
            break;
        }
        $parent = $tree['_index'][$parent->pid];
    }
}

function _private_room_add_file_form_validate($form, &$form_state) {
  $d = _private_room_get_path() . '/g' . $form_state['values']['group'];
  if(file_check_directory($d, FILE_CREATE_DIRECTORY)) {
  
    if (module_exists('transliteration')) // �������������� ���������� ������� � ����� �����
        $_FILES['new_file']['filename'] = transliteration_get($_FILES['new_file']['filename']);

    $file = file_save_upload('new_file', array(), $d, FILE_EXISTS_RENAME);
    if($file === 0) form_set_error('new_file', t('Some error occured when uploading you file'));
    $form_state['storage']['private_room']['file'] = $file;
  }
}

function _private_room_delete_group_confirm_validate($form, &$form_state) {
    // ���� ���� ���� ���� ��������� - ������� ������
    $sql = "SELECT id FROM {private_room_groups} WHERE pid = %d LIMIT 1";
    $result = db_query($sql, $form_state['values']['gid']);
    if( db_affected_rows() > 0)
        form_set_error('group_parent', t('Can\'t delete no empty group. %text', array('%text' => t('Contains subgroups.'))));
    
    // ���� ���� ����� � ������ - ������� ������
    $sql = "SELECT fid FROM {private_room_fg} WHERE gid = %d LIMIT 1";
    $result = db_query($sql, $form_state['values']['gid']);
    if( db_affected_rows() > 0)
        form_set_error('group_parent', t('Can\'t delete no empty group. %text', array('%text' => t('Contains files.'))));
}



/******************FORMS SUBMIT***********************/
function _private_room_admin_form_submit($form, &$form_state) {
  $speed = $form_state['storage']['private_room']['speed'];
  variable_set('private_room_speed', $speed);
  variable_set('private_room_title', check_plain($form_state['values']['private_room_title']));
}

function _private_room_add_group_form_submit($form, &$form_state) {
  db_query("INSERT INTO {private_room_groups} (pid, name, description, function, params, weight) VALUES (%d, '%s', '%s', '%s', '%s', %d)", $form_state['values']['group_parent'], $form_state['values']['group_name'], $form_state['values']['group_desc'], $form_state['storage']['function']['name'], $form_state['storage']['function']['params'], $form_state['values']['group_weight']);
  drupal_set_message(t('Your group has been saved.'));
}

function _private_room_add_file_form_submit($form, &$form_state) {
  $file = $form_state['storage']['private_room']['file'];
  file_set_status($file, FILE_STATUS_PERMANENT);
  _private_room_fg_link($file, $form_state['values']['group'], $form_state['values']['title']);
}

function private_room_group_permissions_form_submit($form, &$form_state) {
  $was = array();
  $sql = "SELECT * FROM {private_room_frp} WHERE 1";
  $result = db_query ($sql);
  while($row = db_fetch_object ($result))
      $was[$row->fid][$row->rid] = $row->id;
  
  $del = array(); $ins = array();
  
  foreach ($form_state['values']['rids'] as $fid => $rids){
      foreach ($rids as $rid => $perm){
          $perm = (boolean)$perm;
          if (isset($was[$fid][$rid]) && !$perm) 
              $del[] = $was[$fid][$rid]; 
          elseif (!isset($was[$fid][$rid]) && $perm)
              $ins[] = "({$fid}, {$rid}, 1)"; 
      }
  }
  
  // ������ �������� ������
  if( count($del) > 0){
      $ids = join($del, ', ');
      $sql = "DELETE FROM {private_room_frp} WHERE id IN({$ids})";
      //dump::trace($sql);
      $result = db_query($sql);
  }
  // ��������� �����
  if( count($ins) > 0){
      $vals = join ($ins, ', ');
      $sql = "INSERT INTO {private_room_frp} (fid, rid, perm) VALUES {$vals}";
      //dump::trace($sql);
      $result = db_query($sql);
  }
  
  drupal_set_message(t('Files permissions were set successfully for the users groups.'), 'status', FALSE);
}

function private_room_user_permissions_form_submit($form, &$form_state) {
  $account = $form_state['values']['account'];
  $uid = $account->uid;
  
  $en = array();
  $dis = array();
  foreach($form_state['values']['uperms'] as $k=>$v)
    if($v) $en[] = $uid . ', ' . $k . ', 1';
    else $dis[] = $k;
  
  //delete permissions
  if(count($dis)) {
    $sql = 'DELETE FROM {private_room_fup} WHERE uid = %d AND fid IN ('.implode(', ', $dis).')';
    $res = db_query($sql, $uid);
  }
  
  if(count($en)) {
    $sql = 'REPLACE INTO {private_room_fup} (uid, fid, uperm) VALUES (' .implode('),(', $en). ')';
    $res = db_query($sql);
  }
  
  drupal_set_message(t('Files permissions were set successfully for the user <a href="/user/@uid">%username</a>', array('%username'=>$account->name,'@uid' => $uid)), 'status', FALSE);
}

/**
 *   ��������� ��������� � ��������� �����
 */
function _private_room_edit_file_form_submit($form, &$form_state){ 
  $fid = $form_state['values']['fid'];
  $gid = $form_state['values']['gid'];
  $title = $form_state['values']['title'];
  $sql = "UPDATE {private_room_fg} SET title = '%s', gid = %d WHERE fid = %d";
  db_query($sql, $title, $gid, $fid);
  drupal_set_message(t('Saved'));
}

/**
 *   ������� ����
 */
function _private_room_delete_file_confirm($form_state, $fid) {

    $sql = "SELECT fg.fid, fg.title, f.filename FROM {private_room_fg} AS fg, {files} AS f ";
    $sql.= " WHERE fg.fid = %d AND f.fid = fg.fid";
    $result = db_query($sql, $fid);
    $file = db_fetch_object($result);
    
    $form['fid'] = array('#type' => 'value', '#value' => $file->fid);
    $msg =  t('Are you sure you want to delete file "%title" (%name)?', array('%title' => $file->title, '%name' => $file->filename));
    $str = confirm_form($form, $msg, 'private_room/edit');
    return $str;
}

function _private_room_delete_file_confirm_submit($form, &$form_state) {
    $fid = $form_state['values']['fid'];
    // ������� ������ ����� � ������� ������
    $sql = "DELETE FROM {private_room_fg} WHERE fid = %d";
    db_query($sql, $fid);
    // ������� ��������� ����� �����
    $sql = "DELETE FROM {private_room_frp} WHERE fid = %d";
    db_query($sql, $fid);
    // ������� ���������������� ����� �����
    $sql = "DELETE FROM {private_room_fup} WHERE fid = %d";
    db_query($sql, $fid);
    // ������ ������ � ��������� �������
    $file = new stdClass(); 
    $file->fid = $fid;
    file_set_status(&$file, 0);
    drupal_set_message(t('Deleted'));
    $form_state['redirect'] = 'private_room/edit';
}

/**
 *   ���������� ��������� ������� ������
 */
function _private_room_edit_group_form_submit($form, &$form_state) {
    //dump::trace($form_state);
    $id = $form_state['values']['id'];
    $sql = "UPDATE {private_room_groups} SET pid = %d, name = '%s', description = '%s', function = '%s', params = '%s', weight = %d WHERE id = %d LIMIT 1";
    db_query($sql, $form_state['values']['group_parent'], $form_state['values']['group_name'], $form_state['values']['group_desc'], $form_state['storage']['function']['name'], $form_state['storage']['function']['params'], $form_state['values']['group_weight'], $id);
    drupal_set_message(t('Your group has been saved.'));
}

function _private_room_delete_group_confirm($form_state, $gid) {
    $sql = "SELECT id, name FROM {private_room_groups} WHERE id = %d";
    $result = db_query($sql, $gid);
    $group = db_fetch_object($result);
    
    $form['gid'] = array('#type' => 'value', '#value' => $group->id);
    $msg =  t('Are you sure you want to delete group "%title"?', array('%title' => $group->name));
    $str = confirm_form($form, $msg, 'private_room/edit');
    return $str;

}

function _private_room_delete_group_confirm_submit($form, &$form_state) {
    $gid = $form_state['values']['gid'];
    // ������� ������ �� ������� ������
    $sql = "DELETE FROM {private_room_groups} WHERE id = %d LIMIT 1";
    db_query($sql, $gid);
    drupal_set_message(t('Deleted'));
    $form_state['redirect'] = 'private_room/edit';
}



/******************PAGE GENERATION***********************/

function _private_room_edit_table() {
  // ������ ������ �����
  $tree = _private_room_group_tree();
  // ������ ����� � ������ �� ������ ����� :)
  $sql = "SELECT * FROM {private_room_fg} WHERE 1 ORDER BY title ASC";
  $result = db_query($sql);
  while ($row = db_fetch_object($result))
    $tree['_index'][$row->gid]->_files[] = $row;

  //dump::trace($tree[0]);
  
  $tb_head = array (t('Title'), t('Actions'));
  $tb_body = _private_room_edit_table_recurs($tree[0]);

  $str = theme_table($tb_head , $tb_body, array ('class' => 'private-room-table'), t('Edit groups and files'));
  return '<div class="private-room">'. $str .'</div>';
}

function _private_room_users_list() {
  $sql = "SELECT uid, name FROM {users} WHERE uid <> 0 AND status = 1";
  $res = pager_query($sql, 50);
  $header = array(t('Username'));
  $rows = array();
  while($row = db_fetch_object($res))
    $rows[] = array(l($row->name,'private_room/edit/uperm/'.$row->uid));
  $str = theme_table($header, $rows, array('class' => 'private-room-users-list-table'), t('Select user to add files permissions'));
  return '<div class="private-room-users-list">'. $str .'</div>';
}